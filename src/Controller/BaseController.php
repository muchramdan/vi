<?php
// src/Controller/BaseController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
    * @Route("/base")
    */
    public function setUserRoleName($roles)
    {
        $roles = $roles['role'];
        return $roles;
    }
    
}
?>