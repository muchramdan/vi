<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use App\Entity\Guest;
use App\Repository\GuestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/virnaikhsanwedding")
*/
class DefaultController extends AbstractController
{
    /**
    * @Route("/", name="index")
    */
    public function indexPage()
    {   
        return $this->render("index.html.twig", array(
            'appdir' => $this->getParameter('kernel.root_dir')
        ));
    }

    /**
     * @Route("/guest/{id}", name="get_oneguest", methods={"GET"})
     */
    public function show($id)
    {
        $guestTmp = array();
        $guest_list = array();

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $sql = "SELECT * FROM guest WHERE id = ".$id;
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        $guest = $stmt->fetchAll();
        foreach ($guest as $value) {
            $guestTmp = array(
                'id' => $value['id'],
                'name' => $value['name']
            );
            array_push($guest_list, $guestTmp);
        }

        $conn->close();
        $em->clear();
        
        return $this->render('guest/guest-page.html.twig', [
            'appdir' => $this->getParameter('kernel.root_dir'),
            'guest' => $guest_list[0]
        ]);
    }
    
}
?>