<?php

namespace App\Entity;

use App\Repository\GuestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GuestRepository::class)
 */
class Guest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $update_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $attending;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer")
     */
    private $session;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $attending_time;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $invitations_path;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAttending(): ?string
    {
        return $this->attending;
    }

    public function setAttending(string $attending): self
    {
        $this->attending = $attending;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSession(): ?int
    {
        return $this->session;
    }

    public function setSession(int $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getAttendingTime(): ?string
    {
        return $this->attending_time;
    }

    public function setAttendingTime(string $attending_time): self
    {
        $this->attending_time = $attending_time;

        return $this;
    }

    public function getInvitationsPath(): ?string
    {
        return $this->invitations_path;
    }

    public function setInvitationsPath(?string $invitations_path): self
    {
        $this->invitations_path = $invitations_path;

        return $this;
    }
}
