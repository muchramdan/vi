// $(document).ready(function () {
	
;(function ($) {
	'use strict';

	var wmedia = window.matchMedia("(max-width: 768px)")
	if (wmedia.matches) {
		$('nav').hide();
	}
  	$(window).scroll(function () {
    	if ($('.navigation').offset().top > 100) {
      		$('.navigation').addClass('fixed-nav');
    	} else {
      		$('.navigation').removeClass('fixed-nav');
    	}
  	});
  

 $('.portfolio-gallery').each(function () {
        $(this).find('.popup-gallery').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });


	$('#contact-form').validate({
		rules: {
			user_name: {
				required: true,
				minlength: 4
			},
			user_email: {
				required: true,
				email: true
			},
			// user_subject: {
			// 	required: false
			// },
			user_message: {
				required: true
			}
		},
		messages: {
			user_name: {
				required: 'Come on, you have a name don\'t you?',
				minlength: 'Your name must consist of at least 2 characters'
			},
			user_email: {
				required: 'Please put your email address'
			},
			user_message: {
				required: 'Put some messages here?',
				minlength: 'Your name must consist of at least 2 characters'
			}

		},
		submitHandler: function (form) {
			$(form).ajaxSubmit({
				type: 'POST',
				data: $(form).serialize(),
				url: 'sendmail.php',
				success: function () {
					$('#contact-form #success').fadeIn();
				},
				error: function () {

					$('#contact-form #error').fadeIn();
				}
			});
		}
	});



	$('.testimonial-slider').slick({
		slidesToShow: 1,
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		dots:true
	});




	// Init Magnific Popup
	$('.portfolio-popup').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: true
		},
		mainClass: 'mfp-with-zoom',
		navigateByImgClick: true,
		arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
		tPrev: 'Previous (Left arrow key)',
		tNext: 'Next (Right arrow key)',
		tCounter: '<span class="mfp-counter">%curr% of %total%</span>',
		zoom: {
			enabled: true,
			duration: 300,
			easing: 'ease-in-out',
			opener: function (openerElement) {
				return openerElement.is('img') ? openerElement : openerElement.find('img');
			}
		}
	});




	var map;

	function initialize() {
		var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(50.97797382271958, -114.107718560791)
			// styles: style_array_here
		};
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	}

	var google_map_canvas = $('#map-canvas');

	if (google_map_canvas.length) {
		google.maps.event.addDomListener(window, 'load', initialize);
	}

	// custom countdown
	// Set the date we're counting down to
	var countDownDate = new Date("Aug 2, 2020 08:00:00").getTime();

	// Update the count down every 1 second
	var x = setInterval(function() {

	// Get today's date and time
	var now = new Date().getTime();
		
	// Find the distance between now and the count down date
	var distance = countDownDate - now;
		
	// Time calculations for days, hours, minutes and seconds
	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
	// Output the result in an element with id="demo"
	// document.getElementById("custom_countdown").innerHTML = days + "d " + hours + "h "
	// + minutes + "m " + seconds + "s ";

	document.getElementById("custom_countdown").innerHTML = "" 
	+ "<div class='countdown_section'>"
		+ "<span class='countdown_amount'>" + days + "</span>"
		+ "<span class='countdown_period'>DAYS</span>"
	+ "</div>"
	+ "<div class='countdown_section'>"
		+ "<span class='countdown_amount'>" + hours + "</span>"
		+ "<span class='countdown_period'>HOURS</span>"
	+ "</div>"
	+ "<div class='countdown_section'>"
		+ "<span class='countdown_amount'>" + minutes + "</span>"
		+ "<span class='countdown_period'>MINUTES</span>"
	+ "</div>"
	+ "<div class='countdown_section'>"
		+ "<span class='countdown_amount'>" + seconds + "</span>"
		+ "<span class='countdown_period'>SECONDS</span>"
	+ "</div>";
		
	// If the count down is over, write some text 
	if (distance < 0) {
		clearInterval(x);
		document.getElementById("custom_countdown").innerHTML = "<h2 style='color: #fff;' class='text-shadow-1'>It's Our Happy Day!</h2>";
	}
	}, 1000);

	// disable right-click, supaya gambar tidak dapat di save oleh viewers
// 	$("body").on("contextmenu",function(e){
// 		return false;
//    });

})(jQuery);
